package com.soonur;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class MyClass {

    private String name;
    private String surname;

    public MyClass() {
        System.out.println("MyClass instance created.");
        log.info("test");
    }
}
