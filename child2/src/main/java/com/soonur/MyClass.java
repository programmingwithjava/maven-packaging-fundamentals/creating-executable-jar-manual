package com.soonur;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.BasicConfigurator;
import org.joda.time.DateTime;

@Data
@Slf4j
public class MyClass {

    private String name;
    private String surname;
    private DateTime currentDate;

    public MyClass() {
        BasicConfigurator.configure();
        log.info("MyClass instance created.");
        currentDate = new DateTime();
    }

    public void printTime(){
        System.out.println("JODA TIME - centuryOfEra: " + currentDate.centuryOfEra().getAsString() + " dayOfMonth: " + currentDate.dayOfMonth().getAsString() + " dayOfWeek: " +currentDate.dayOfWeek().getAsString() + " dayOfYear: " + currentDate.dayOfYear().getAsString());
    }
}
